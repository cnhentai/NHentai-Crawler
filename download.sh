#!/bin/bash
source setting.cfg
source "${2}"
if [ -f "${1}/stop.eh" ]
then
    warn_banned="false"
else
    warn_banned="true"
fi
grep -F "${title}" ${download_db}>/dev/null && exit
curl --retry 5 --retry-connrefused --tcp-fastopen -s "https://i.nhentai.net/galleries/${target}/1.jpg" >"${1}/galleryentry/${title}.tmp"
grep "404" "${1}/galleryentry/${title}.tmp" && (
    touch "${1}/${title}.png"
)
if [ -f "${1}/${title}.png" ]
then
    image_format="png"
    rm "${1}/${title}.png"
else
    image_format="jpg"
fi
if [ ! -s "${1}/galleryentry/${title}.tmp" ]
then
    if ${warn_banned}
    then
        if [ ! -f "${1}/stop.eh" ]
        then
            echo "Unable to download Gallery Entry. You might have been banned."
        fi
    fi
    exit
fi
grep "Your IP address has been temporarily banned for" "${1}/galleryentry/${title}.tmp" && (
    if ${warn_banned}
    then
        if [ ! -f "${1}/stop.eh" ]
        then
            echo "You have been banned."
        fi
    fi
    exit
)
touch "${1}/${title}.stay"
currnt_page=1
mkdir "${1}/saved_comic/${title}"
while [ -f "${1}/${title}.stay" ] 
do
    curl --retry 5 --retry-connrefused --tcp-fastopen -s -o "${1}/saved_comic/${assigned}/${currnt_page}.${image_format}" "https://i.nhentai.net/galleries/${target}/${currnt_page}.${image_format}"
    grep "404" "${1}/galleryentry/${assigned}.tmp" && (
        rm "${1}/${title}.stay"
        rm "${1}/saved_comic/${title}/${currnt_page}.${image_format}"
    )
    currnt_page=`expr ${currnt_page} + 1`
done
if [ ! -f "${1}/saved_comic/${assigned}/1.jpg" ]
then
    rm -r "${1}/saved_comic/${assigned}"
else
    rm "${1}/galleryentry/${assigned}.tmp"
    (
        mv -u "${1}/saved_comic/${assigned}" "${download_directory}/${title}"
    ) &
    (
        echo "${title}" >>${download_db}
    ) &
    wait
    echo "Downloaded ${title} (format: ${image_format})..."
fi
exit