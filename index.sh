#!/bin/bash
source setting.cfg
if [ -f "${1}/stop.eh" ]
then
    warn_banned="false"
else
    warn_banned="true"
fi
url="https://nhentai.net/${search_string}/?page=${2}"
curl --retry 64 --retry-connrefused --tcp-fastopen -s "${url}" >"${1}/searchpage/${2}.tmp"
if [ ! -s "${1}/searchpage/${2}.tmp" ]
then
    if ${warn_banned}
    then
        if [ ! -f "${1}/stop.eh" ]
        then
            touch "${1}/stop.eh"
            echo "Unable to download page ${2}. You might have been banned."
        fi
    fi
    exit
fi
grep "Your IP address has been temporarily banned for" "${1}/searchpage/${2}.tmp" && (
    if ${warn_banned}
    then
        if [ ! -f "${1}/stop.eh" ]
        then
            touch "${1}/stop.eh"
            echo "Unable to download page ${2}. Your have been banned."
        fi
    fi
    exit
)
echo "Downloading Page ${2}..."
cat "${1}/searchpage/${2}.tmp" | while read line
do
    if [ "${line}" != "" ]
    then
        if [ "${line##*div*gallery*thumb*}" == "" ]
        then
            line_2="${line##*div*data?src??*nhentai*galleries?}"
            line_target="${line_2%%?thumb*alt*}"
            line_title_d="${line##?div?class??gallery*a?href*class*noscript??div?class??caption??}"
            line_title="${line_title_d%%??div???a???div?}"
            echo -n "filename=" >"${1}/filename/${2}.tmp"
            echo -n "${line_target}"|md5sum|cut -d ' ' -f 1 >>"${1}/filename/${2}.tmp"
            source "${1}/filename/${2}.tmp"
            rm "${1}/filename/${2}.tmp"
            line_title=${line_title//\//_}
            line_title=${line_title:0:230}
            echo "title=\"${line_title}\"" >${index_directory}/${filename}.index
            echo "target=\"${line_target}\"" >>${index_directory}/${filename}.index
        fi
    fi
done
rm "${1}/searchpage/${2}.tmp"
exit